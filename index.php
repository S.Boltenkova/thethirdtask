<link rel="stylesheet" type="text/css" href="style.css" >
<div class ="main-form">
<div class = "text-field">
<form action="form.php" method="POST">
  <label>
      Input name:<br />
      <input name ="name" type="text" placeholder=" Name"> 
  </label><br>
  <label>
      Input e-mail:<br>
      <input name="email" type="text" placeholder=" e-mail">
  </label><br>
  <label>
      Input the year of birth<br>
      <select name="year">
    <?php for($i = 1900; $i < 2020; $i++) { ?>
    <option value="<?php print $i; ?>"><?= $i; ?></option>
    <?php } ?>
  </select><br>
</label>
<br>
      Choose gender: <br>
      <label><input type="radio" name="gender" value="Female" checked="checked"> Female </label>
      <label><input type="radio" name="gender" value="Male" checked="checked"> Male </label><br><br>
      Choose the number of limbs:<br>
      <label><input type="radio" name="limbs" value="0" checked="checked"> 0 </label>
      <label><input type="radio" name="limbs" value="1"> 1 </label>
      <label><input type="radio" name="limbs" value="2"> 2 </label>
      <label><input type="radio" name="limbs" value="3"> 3 </label>
      <label><input type="radio" name="limbs" value="4"> 4 </label>
      <label><input type="radio" name="limbs" value="more">More</label><br>
  <label>
      Choose superpowers:<br>
      <select name="superpowers[]" multiple="multiple">
          <option value="Immortality">Immortality</option>
          <option value="Passing through walls">Passing through walls</option>
          <option value="Levitation">Levitation</option>
          <option value="Mind reading">Mind reading</option>
          <option value="Teleportation">Teleportation</option>
      </select>
  </label><br>
  <label>
      Tell us about yourself
      <br><textarea id="main" name="bio" placeholder=" Biografy"></textarea>
  </label><br>
  <label>
      <input name="agree" type="checkbox">
      I agree to the terms and privacy policy
  </label><br>
  <input type="submit" value="Send">
</form>